(function(CrunchSS, $){


	var Carousel = function(options) {

		this.options = $.extend({
			carouselWindowClass: 			'js-carousel__window',
			carouselSliderClass: 			'js-carousel__slider',
			carouselAnimateCSSClass: 	'animate-css',
			carouselAnimateJSClass: 	'animate-js',

			navLeftClass: 		'js-carousel__prev',
			navRightClass: 		'js-carousel__next',
			navDisabledClass: 'disabled',

			panelClass: 				'js-carousel__panel',
			panelHideClass: 		'invisible',
			panelVisibleClass: 	'visible',

			progressBarId: 		'js-progress-',
			progressBarClass: 'js-progress',

			touchMoveXMin: 10,
			touchMoveYMax: 200,
			touchTimeMax:  2000,

			transitionLength: 400, // Length of CSS/JS transitions
			debounceDelay: 		400,

		}, options);


		this.carousel 			= null;
		this.carouselWidth 	= null;

		this.carouselWindow	= null;

		this.carouselSlider 			= null;
		this.carouselSliderWidth  = null;
		this.carouselSliderPos 		= null;

		this.progressBar 			= null;

		this.panels 			= undefined;
		this.currentPanel = undefined;
		this.nextPanel 		= undefined;
		this.prevPanel 		= undefined;
		this.showPanel 		= undefined;
		this.numberPanelsVisible 	= 0;
		this.panelPadding = 0;

		this.navLeft 	= null;
		this.navRight = null;

		this.touchStartX 		= null;
		this.touchStartY 		= null;
		this.touchStartTime = null;
		this.touchMoveX 		= null;
		this.touchMoveY 		= null;
		this.touchDirection = null;

		this.isMobile 				= false;
		this.isTouchEnabled 	= Modernizr.touchevents;
		this.isCssAnimations 	= Modernizr.csstransforms3d && Modernizr.csstransitions;
		this.isRangeInputs 		= Modernizr.inputtypes.range;

		this.carouselAnimateClass = null;
	};

	Carousel.prototype = {


		/*
		 ***************************************************************************
		 * Carousel
		 ***************************************************************************
		 */


		carouselInit: function(carousel) {
			this.carousel = carousel;
			this.carouselWindow = carousel.children('.'+this.options.carouselWindowClass);
			this.carouselSlider = this.carouselWindow.children('.'+this.options.carouselSliderClass);

			this.carouselAnimations();
			this.carouselWidths();
			this.carouselHeight();
		},


		carouselAnimations: function() {
			// Check for support of CSS transtitions and transforms
			if(this.isCssAnimations) {
				this.carouselAnimateClass = this.options.carouselAnimateCSSClass;
			} else {
				this.carouselAnimateClass = this.options.carouselAnimateJSClass;
			}
			this.carouselSlider.addClass(this.carouselAnimateClass);
		},


		carouselPos: function() {
			this.showPanel = this.currentPanel;
			this.move();
		},


		carouselWidths: function() {
			this.carouselWidth = this.carouselWindow.width();
			this.carouselSliderWidth = this.carouselSlider.width();
		},

		carouselHeight: function() {
			var carouselSliderHeight = this.carouselSlider.outerHeight(true);
			this.carouselWindow.css('height', carouselSliderHeight);
		},


		/*
		 ***************************************************************************
		 * Panels
		 ***************************************************************************
		 */


		panelsInit: function() {
			this.panels = this.carouselSlider.children('.'+this.options.panelClass);
			this.panelsVisible();
			this.panelStatus();
			this.panelPaddingWidth();
		},


		panelStatus: function() {
			// Check if current panel is defined or if animate class been removed for resizing
			if ( this.currentPanel === undefined || ! this.carouselSlider.hasClass(this.carouselAnimateClass) ) {
				this.panelsVisible();
				this.currentPanel = this.carouselSlider.children('.'+this.options.panelVisibleClass).first().index();
			}

			this.prevPanel = parseInt(this.currentPanel) - 1;
			this.nextPanel = parseInt(this.currentPanel) + 1;
		},


		panelsVisible: function() {

			var carouselPosLeft 	= this.carouselWindow.offset().left;
			var carouselPosRight 	= this.carouselWidth + carouselPosLeft;
			var panels 						= this.panels;

			var panelPosLeft, panelPosRight, $panel;

			this.numberPanelsVisible = panels.length;

			for (var i = 0, len = panels.length; i < len; i++) {

				$panel = $(panels[i]);
				panelPosLeft  = $panel.offset().left;
				panelPosRight = $panel.width() + panelPosLeft;

				if (panelPosRight > carouselPosRight || panelPosLeft < carouselPosLeft) {
					$panel.removeClass(this.options.panelVisibleClass);
					this.numberPanelsVisible -= 1;
					//$panel.addClass(this.option.panelHideClass); // Check hiding of elements
				} else {
					$panel.addClass(this.options.panelVisibleClass);
					//$panel.removeClass(this.option.panelHideClass); // Check hiding of elements
				}
			}
		},

		panelPaddingWidth: function() {
			// If mobile work out left over space either side of panel
			if (this.isMobile) {
				this.panelPadding = (this.carouselWidth - this.panels.eq(this.currentPanel).outerWidth(false) ) / 2;
			} else {
				this.panelPadding = 0;
			}
		},


		/*
		 ***************************************************************************
		 * Navigation
		 ***************************************************************************
		 */


		navInit: function() {
			var carousel = this;

			this.navInsert();

			this.navLeft = this.carouselWindow.siblings('.'+this.options.navLeftClass);
			this.navRight = this.carouselWindow.siblings('.'+this.options.navRightClass);

			this.navLeft.bind('click', function(e) {
				e.preventDefault();
				if (! carousel.navLeft.hasClass(carousel.options.navDisabledClass)) {
					carousel.showPanel = carousel.prevPanel;
					carousel.move();
				}
			});

			this.navRight.bind('click', function(e) {
				e.preventDefault();
				if (! carousel.navRight.hasClass(carousel.options.navDisabledClass)) {
					carousel.showPanel = carousel.nextPanel;
					carousel.move();
				}
			});



			this.navStatus();

			// Set up touch nav
			this.navTouchInit();
		},


		navInsert: function() {
			var output;
			output  = '<div class="js-carousel__prev c-carousel__nav-prev c-carousel__nav"><button class="c-carousel__button"></button></div>';
			output += '<div class="js-carousel__next c-carousel__nav-next c-carousel__nav"><button class="c-carousel__button"></button></div>';

			return $(output).prependTo(this.carousel);
		},


		navStatus: function (){

			this.navLeft.addClass(this.options.navDisabledClass);
			this.navRight.addClass(this.options.navDisabledClass);

			if ( this.currentPanel > 0 ) {
				this.navLeft.removeClass(this.options.navDisabledClass);
			}

			if (this.currentPanel < this.panels.length - this.numberPanelsVisible ) {
				this.navRight.removeClass(this.options.navDisabledClass);
			}

			if (this.navLeft.hasClass(this.options.navDisabledClass) && this.navRight.hasClass(this.options.navDisabledClass)) {
				this.navLeft.hide();
				this.navRight.hide();
			} else {
				this.navLeft.show();
				this.navRight.show();
			}

		},



		/*
		 ***************************************************************************
		 * Navigation - Touch
		 ***************************************************************************
		 */


		navTouchInit: function() {
			if (! this.isTouchEnabled) {
				return false;
			}

			var self = this;

			if (this.carouselSliderWidth <= this.carouselWidth) {
				this.carouselSlider.unbind('touchstart');
				this.carouselSlider.unbind('touchmove');
				this.carouselSlider.unbind('touchend');

			} else {
				this.carouselSlider.bind('touchstart', function(event) {
					self.navTouchStart(event);
				});

				this.carouselSlider.bind('touchmove', function(event) {
					self.navTouchMove(event);
				});

				this.carouselSlider.bind('touchend', function(event) {
					self.navTouchEnd(event);
				});
			}
		},


		navTouchStart: function(event) {
			// Collect the starting touch conditions
			this.touchStartX =  event.originalEvent.touches[0].pageX;
			this.touchStartY =  event.originalEvent.touches[0].pageY;
			this.touchStartTime = new Date().getTime();

			// Remove animation for smooth action
			this.carouselSlider.removeClass(this.carouselAnimateClass);
		},


		navTouchMove: function(event) {
			var showPanel 					= this.panels.eq(this.currentPanel);
			var showPanelPos 				= showPanel.position().left;
			var showPanelWidthInner = showPanel.outerWidth(false);
			var showPanelWidthOuter = showPanel.outerWidth(true);
			var distanceX;
			var distanceY;
			var elapsedTime;

			// Continuously return touch position.
			this.touchMoveX = event.originalEvent.touches[0].pageX;
			this.touchMoveY = event.originalEvent.touches[0].pageY;

			// Calculate comparison vars
			distanceX = Math.abs(this.touchMoveX - this.touchStartX);
			distanceY = Math.abs(this.touchMoveY - this.touchStartY);
			elapsedTime = new Date().getTime() - this.touchStartTime;

			// Get panels position without margins
			showPanelPos += showPanelWidthOuter - showPanelWidthInner;

			// Check touch is good
			if (distanceX >= this.options.touchMoveXMin &&
					distanceY <= this.options.touchMoveYMax &&
					elapsedTime <= this.options.touchTimeMax) {

				event.preventDefault();

				// Calculate new pos based on touch
				showPanelPos = showPanelPos + (this.touchStartX - this.touchMoveX);

				if (showPanelPos - this.panelPadding >= this.carouselSliderWidth - this.carouselWidth + this.panelPadding ) {
					this.carouselSliderPos = (this.carouselWidth - this.carouselSliderWidth) - this.panelPadding;
					this.showPanel = this.panels.length - 1;
				} else if (showPanelPos < 0) {
					this.carouselSliderPos = 0 + this.panelPadding;
					this.showPanel = 0;
				} else {
					this.carouselSliderPos = (Math.abs(showPanelPos) * -1) + this.panelPadding;
					this.showPanel = Math.round(Math.abs(this.carouselSliderPos / (this.carouselSliderWidth / (this.panels.length ) ) ), 0 );
				}

				// Use prefered movement method
				this.moveAction();
			}
		},


		navTouchEnd: function() {
      // Animate and move the elements.
			this.carouselSlider.addClass(this.carouselAnimateClass);
			this.move();
		},



		/*
		 ***************************************************************************
		 * Progress Bar
		 ***************************************************************************
		 */


		progressInit: function(index) {

			// If browser doesn't support range inputs don't show progress bar
			if (! this.isRangeInputs) {
				return;
			}

			var self = this;

			this.progressInsert();

			// Create unique id for progress to be used when updating inner width
			this.options.progressBarId += index;

			this.progressBar 	= this.carouselWindow.siblings('.'+this.options.progressBarClass);
			this.progressBar.attr('id', this.options.progressBarId);

			this.progressProps();

			// Input fires as the progressBar/range input is moved
			this.progressBar.on('input', function() {
				self.showPanel = this.value;
				self.move();
			});

			// 'change' behaves as 'input' in IE
			// Normally fires on mouseup in sane browsers
			this.progressBar.on('change', function() {
				self.showPanel = this.value;
				self.move();
			});

			// this.progressBar.on('click', function(event) {
			// 	var progressBarWidth = $(this).innerWidth(); //use .outerWidth() if you want borders
			// 	var progressBarOffset = $(this).offset();
			// 	var progressBarClickPos = event.pageX - progressBarOffset.left;
			// 	if ( progressBarWidth/2 > progressBarClickPos ) {
			// 		self.showPanel = self.prevPanel;
			// 		self.move();
			// 	} else {
			// 		self.showPanel = self.nextPanel;
			// 		self.move();
			// 	}
			// });

		},

		progressProps: function() {
			// If browser doesn't support range inputs don't show progress bar
			if (! this.isRangeInputs) {
				return;
			}

			this.progressWidth();

			this.progressBar.prop({
				min: 0,
				max: this.panels.length - this.numberPanelsVisible,
				value: this.currentPanel,
				step: 1
			});
		},


		progressInsert: function() {
			var output = '<input type="range" id="js-progress" class="js-progress c-progress-bar">';
			return $(output).appendTo(this.carousel);
		},


		progressWidth: function (){

			var progressBarInnerWidth = (this.progressBar.width() / this.panels.length) * this.numberPanelsVisible ;

			if (progressBarInnerWidth >= this.progressBar.width()) {
				this.progressBar.hide();
			} else {
				this.progressBar.show();
				// Cannot add styles to ::psuedo-elements.
				// Add inline style to head instead
				$('<style>#'+this.options.progressBarId+'::-webkit-slider-thumb{width:'+progressBarInnerWidth+'px}</style>').appendTo('head');
				$('<style>#'+this.options.progressBarId+'::-moz-range-thumb{width:'+progressBarInnerWidth+'px}</style>').appendTo('head');
				$('<style>#'+this.options.progressBarId+'::-ms-thumb{width:'+progressBarInnerWidth+'px}</style>').appendTo('head');

			}


		},


		progressStatus: function () {
			// If browser doesn't support range inputs don't show progress bar
			if (! this.isRangeInputs) {
				return;
			}
			this.progressBar.val(this.currentPanel);
		},

		/*
		 ***************************************************************************
		 * General Behaviour
		 ***************************************************************************
		 */


		checkMobile: function() {
			this.isMobile = (returnCurrentBreakpoint() === 'default-screen');
		},


		resize: function() {
			var self = this;
			$(window).bind('resize', function() {

				// Remove animation class
				self.carouselSlider.removeClass(self.carouselAnimateClass);

				// Check new viewport size
				self.checkMobile();

				// Set carousel widths based on viewport
				self.carouselWidths();

				// Set carouselHeight
				self.carouselHeight();

				// Set current/next/prev panel
				self.panelStatus();

				// Set panel padding for mobile
				self.panelPaddingWidth();

				// Set nav disabled
				self.navStatus();

				// Initialize nav touch again if required
				self.navTouchInit();

				// Check current slide position is correct
				self.carouselPos();

				// Set progress based on viewport only if browser supports range input
				self.progressProps();

				// Add animation class
				self.carouselSlider.addClass(self.carouselAnimateClass);
			});
		},


		move: function() {

			var carouselWidth = this.carouselWidth;

			var carouselSliderWidth = this.carouselSliderWidth;
			var newcarouselSliderPos = 0;
			var showPanel = this.panels.eq(this.showPanel);
			var showPanelPos = showPanel.position().left;
			var showPanelWidthInner = showPanel.outerWidth(false);
			var showPanelWidthOuter = showPanel.outerWidth(true);

			// Get panels position and new carousel position
			showPanelPos += showPanelWidthOuter - showPanelWidthInner;
			newcarouselSliderPos = Math.abs(showPanelPos) * -1;

			newcarouselSliderPos += this.panelPadding;

			// If not mobile and slider is smaller than carousel just set pos to 0
			if (! this.isMobile && carouselSliderWidth <= carouselWidth) {
				newcarouselSliderPos = 0;
			// If not mobile and new position takes right of slider away from right of
			// carousel window, set pos to be max possible
			} else if (! this.isMobile &&  newcarouselSliderPos + carouselSliderWidth <= carouselWidth) {
				newcarouselSliderPos = (carouselWidth - carouselSliderWidth);
			// If mobile and new position is a positive number set to zero and add padding
			} else if (this.isMobile && newcarouselSliderPos >= 0) {
				newcarouselSliderPos = 0 + this.panelPadding;
			}

			this.carouselSliderPos = newcarouselSliderPos;
			this.currentPanel 	= this.showPanel;

			debounce(this.moveAction(), this.options.debounceDelay, true);

			this.panelStatus();

			this.navStatus();

			this.progressStatus();

			this.panelPaddingWidth();
		},


		moveAction: function() {

			var position = this.carouselSliderPos;

			if (this.isCssAnimations ) {
				this.carouselSlider.css('transform', 'translate3d('+position+'px, 0, 0)');
			} else if ( this.carouselSlider.hasClass(this.carouselAnimateClass) ) {
				this.carouselSlider.animate({'left': position}, this.options.transitionLength);
			} else {
				this.carouselSlider.css('left', position);
			}

		},


		init: function(carousel,  index) {

			if (carousel === undefined) {
				throw new Error('Carousel initialisation failed - carousel object not specified');
			} else if (index === undefined) {
				throw new Error('Carousel initialisation failed - carousel index not specified');
			}

			// Check new viewport size
			this.checkMobile();

			// Set up carousel
			this.carouselInit(carousel);

			// Set up panels
			this.panelsInit();

			// Set up nav
			this.navInit();

			// Set up progress bar
			this.progressInit(index);

			// Check initial carousel position
			this.carouselPos();

			// Set listener to recal widths and pos on resize
			this.resize();
		}
	};

	$(window).load(function () {

		if ($('.js-carousel').length > 0) {
			$('.js-carousel').each(function(index) {
				var carousel = new Carousel();
				carousel.init($(this), index);
			});
		}
	});
})(window.CrunchSS = window.CrunchSS || {}, jQuery);
