(function(CrunchSS, $){

var strokeAnimationduration = 500; // this animation should last for 2 seconds
var fillAnimationduration = 2000; // this animation should last for 2 seconds

function initAreaGraph(thisGraph) {
  var graphStrokes = thisGraph ? [thisGraph] : Array.prototype.slice.call(document.querySelectorAll('path.graph-stroke'));
  graphStrokes.forEach(function(elem){
    var path = elem;
    var length = path.getTotalLength();
    path.style.strokeDasharray = length + ' ' + length; 
    path.style.strokeDashoffset = length;
    path.style.opacity = 1;
  });
}

function animateAreaGraph(graphGroup,graphFillOpacity,callback) {
  animateAreaGraphStroke(graphGroup,graphFillOpacity,animateAreaGraphFill)
}

function animateAreaGraphStroke(graphGroup,graphFillOpacity,callback) {

  var animationHandle;
  var fillGraph = graphGroup.querySelector('path.graph-fill');
  var strokeGraph = graphGroup.querySelector('path.graph-stroke');
  var length = strokeGraph.getTotalLength();

  fillGraph.style.opacity = 0;
    
 initAreaGraph(strokeGraph);
  
  var initial_ts1 = new Date().getTime();
  
  var drawGraphStroke = function() {
     var progress = (Date.now() - initial_ts1)/strokeAnimationduration;
     if (progress >= 1) {
       window.cancelAnimationFrame(animationHandle);
       callback(graphGroup,graphFillOpacity);
     } else {
       strokeGraph.style.strokeDashoffset = Math.floor(length * (1 - progress));
       animationHandle = window.requestAnimationFrame(drawGraphStroke);
     }
  };

  drawGraphStroke();

}

function animateAreaGraphFill(graphGroup,finalOpacity) {
  
  var animationHandle;
  var fillGraph = graphGroup.querySelector('path.graph-fill');
  var currentOpacity = fillGraph.style.opacity = 0;

  incrementalOpacity = (finalOpacity - currentOpacity)/(fillAnimationduration/100);
  
  var drawGraphFill = function() {
    if (currentOpacity >= finalOpacity) {
       window.cancelAnimationFrame(animationHandle);
     } else {
       currentOpacity += incrementalOpacity;;
       fillGraph.style.opacity = currentOpacity;
       animationHandle = window.requestAnimationFrame(drawGraphFill);
     }
  }
  drawGraphFill();
}

function animateAreaChart(areaChartContainer){
  animateAreaGraph(areaChartContainer.querySelector('.graph1'),1);
  setTimeout(function(){
    animateAreaGraph(areaChartContainer.querySelector('.graph2'),0.8)  ;
  },250);
}

function animateAreaChart1(){
  var areaChartContainer = document.querySelector('#area-chart1');
  animateAreaChart(areaChartContainer);
}

function animateAreaChart2(){
  var areaChartContainer = document.querySelector('#area-chart2');
  animateAreaChart(areaChartContainer);
}

function animateBarChart(){
    
    var animationHandle;
    var barChartContainer = document.querySelector('.stat-chart.bar-chart svg')
    var bars = Array.prototype.slice.call(barChartContainer.querySelectorAll('rect'));
    var chartHeight = barChartContainer.viewBox.baseVal.height;

    bars.forEach(function(bar){
      
      var barHeight = bar.height.baseVal.value;
      var heightIncRate = barHeight/(fillAnimationduration/100);
      var currentHeight = 0;
      var finalHeight = barHeight;

      bar.style.opacity = 1;
      
      bar.height.baseVal.value = currentHeight;
      bar.y.baseVal.value = chartHeight - currentHeight;

      var animateBars = function() {
        if(currentHeight >= finalHeight) {
          currentHeight = finalHeight;
          window.cancelAnimationFrame(animationHandle);
        } else {
          currentHeight += heightIncRate;
          bar.height.baseVal.value = currentHeight;
          bar.y.baseVal.value = chartHeight - currentHeight;
          animationHandle = window.requestAnimationFrame(animateBars);
        }
      }

      window.setTimeout(animateBars,250);

    });

}

function animatePieChart() {
  
  var animationHandle;
  var pieChartContainer = document.querySelector('.stat-chart.pie-chart svg')
  var pieSegments = Array.prototype.slice.call(pieChartContainer.querySelectorAll('path'));
  var seqExecutor = [];


  pieSegments.forEach(function(pieSegment){

    var length = pieSegment.getTotalLength();
    var dashoffsetDecRate = length/(fillAnimationduration/100);
    var currentDashoffset = length;
    var finalDashoffset = 0;

    pieSegment.style.strokeDasharray = length + ' ' + length; 
    pieSegment.style.strokeDashoffset = currentDashoffset;
    pieSegment.style.opacity = 1;
  
    var animatePieSegment = function() {
       if (currentDashoffset <= finalDashoffset) {
         pieSegment.style.strokeDashoffset = finalDashoffset;
         window.cancelAnimationFrame(animationHandle);
         //seqExecutor.pop()();
       } else {
         currentDashoffset -= dashoffsetDecRate;
         pieSegment.style.strokeDashoffset = currentDashoffset;
         animationHandle = window.requestAnimationFrame(animatePieSegment);
       }
    };
    //seqExecutor.push(animatePieSegment);
    window.setTimeout(animatePieSegment,250);

  });

}


CrunchSS.animateAreaChart1 = animateAreaChart1;
CrunchSS.animateAreaChart2 = animateAreaChart2;
CrunchSS.animateBarChart = animateBarChart;
CrunchSS.animatePieChart = animatePieChart;

})(window.CrunchSS = window.CrunchSS || {}, jQuery);
